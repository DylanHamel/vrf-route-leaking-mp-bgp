# VRF - Route Leaking with MP-BGP

Dylan Hamel - <dylan.hamel@protonmail.com>



## Schema

![vrf_network](./schema/vrf_network.png)



## Basic Configuration

#### ClientA

```
interface Loopback0
 ip address 1.1.1.1 255.255.255.0

interface FastEthernet0/0
 ip address 10.1.1.1 255.255.255.0
 duplex auto
 speed auto
 no shutdown

ip route 0.0.0.0 0.0.0.0 10.1.1.254 name default-gateway
```



#### ClientB

```
interface Loopback0
 ip address 2.2.2.1 255.255.255.0

interface FastEthernet0/1
 ip address 10.2.2.1 255.255.255.0
 duplex auto
 speed auto
 no shutdown

ip route 0.0.0.0 0.0.0.0 10.2.2.254 name default-gateway
```



#### Internet

```
ip vrf CLIENTA
ip vrf CLIENTB
```

```
interface FastEthernet0/0
 description Link-ClientA
 ip vrf forwarding CLIENTA
 ip address 10.1.1.254 255.255.255.0
 duplex auto
 speed auto
 no shutdown

interface FastEthernet0/1
 description Link-ClientB
 ip vrf forwarding CLIENTB
 ip address 10.2.2.254 255.255.255.0
 duplex auto
 speed auto
 no shutdown
```

```
ip route vrf CLIENTA 1.1.1.0 255.255.255.0 10.1.1.1
ip route vrf CLIENTB 2.2.2.0 255.255.255.0 10.2.2.1
```



## MP-BGP Configuration 

Actuellement, les VRF ne peuvent pas communiquer entre eux. Pour cela, il faut configurer les VRF avec un ```Route Targer``` contenant un ```import``` et un ```export``` différent.

C'est à dire que les routes seront importées d'un VRF et exportées dans un autre.



#### Configuration VRF

* VRF CLIENTA => ```rd 1:1``` car ```ip address 1.1.1.1 255.255.255.0```
* VRF CLIENTB => ```rd 2:2``` car ```ip address 2.2.2.1 255.255.255.0```

```
ip vrf CLIENTA
 rd 1:1
 route-target import 2:2
 route-target export 1:1
```

```
ip vrf CLIENTB
 rd 2:2
 route-target import 1:1
 route-target export 2:2
```



#### Configuration BGP

> Configure VRF first
>
> Internet1(config-router)#address-family ipv4 vrf CLIENTA
> % VRF CLIENTA does not have an RD configured.

```
router bgp 4
 no synchronization
 bgp router-id 100.100.100.100
 bgp log-neighbor-changes
 no auto-summary
 
 address-family ipv4 vrf CLIENTB
  redistribute connected
  redistribute static
  no synchronization
 exit-address-family
 
 address-family ipv4 vrf CLIENTA
  redistribute connected
  redistribute static
  no synchronization
 exit-address-family
```

La configuration est beaucoup plus simple que le ```Route Leaking Static```

```
ClientB#ping 1.1.1.1 source 2.2.2.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
Packet sent with a source address of 2.2.2.1
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 88/90/96 ms
```

